"use strict";

/**
 * Declare a module called 'myApp', with no dependencies 
 */
angular.module('myApp', [])
    /**
     * extend module myApp, with a controller named 'NameCtrl' 
     * that has $scope and PeopleService as dependencies
     * 
     */
    .controller('NameCtrl', function($scope, PeopleService) {
        $scope.name = 'bengt';

        $scope.people = PeopleService.list();

        $scope.save = function() {
            alert($scope.name);
        }
    });

/**
 * Get previously defined module 'myApp'
 */
angular.module('myApp')
    /**
     * Extend module 'myApp' with a service
     */
    .service('PeopleService', function() {
        this.list = function() {
            return [
                {
                    Name : 'Åke',
                    Age : 35
                },
                {
                    Name : 'Gösta',
                    Age : 78
                }
            ]
        }
    });

/**
 * Get previously defined module 'myApp'
 */
angular.module('myApp')
    /**
     * Extend module 'myApp' with a directive
     */
    .directive('myDirective', function() {
        return {
            restrict: 'A',
            template: 'Hello, I am a directive'
        };
    });

/**
 * Get previously defined module 'myApp'
 */
angular.module('myApp')
    /**
     * Extend module 'myApp' with a filter
     */
    .filter('reverse', function() {
        return function(input) {
           return input.split('').reverse().join('');
        };
    });